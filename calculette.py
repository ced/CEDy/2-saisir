def affiche_cout(tab, nb_tab):
    for e in tab:
        print("\t", e, end="\t"*nb_tab)
    print()


def calcule_cout_km(les_couts, km):
    res = [0, 0]   # Initialiser la liste des résultats
    indice = 0
    for un_cout in les_couts:
        res[indice] = round(un_cout * km, 1)
        indice = indice + 1
    return res


def choix_entier(message):
    return int(input(message))


train = [14.62, 9.26]
en_tete = """\
Calculette : Eco-déplacements.
Calculez l'impact de vos déplacements quotidiens
sur l'environnement et vos dépenses"""
bilan = "Bilan annuel des coûts pour ce mode de déplacement :"
en_tete_cout = ["kg eq. CO2", "l eq. pétrole"]
ligne = "#" * 50

distance = """\
Quelle est la distance entre le domicile et le lieu de travail ? """

deplacement = """\
Choisir le mode de deplacement :
1 - train
2 - vélo
3 - voiture

Mon choix : """

print(ligne)
print(en_tete)
print(ligne)
km = choix_entier(distance)
val_dep = choix_entier(deplacement)
print("\n", bilan)
affiche_cout(en_tete_cout, 1)
mes_couts = calcule_cout_km(train, km)
affiche_cout(mes_couts, 2)
