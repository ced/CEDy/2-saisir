**Rappel** : Bien que la calculette de l'Ademe présente une interface graphique (GUI : Graphic User Interface), nous allons dans un premier temps, nous concentrer sur un programme en ligne de commandes (CLI : Command Line Interface).
**Écrire un programme CLI**, par rapport à un un programme GUI, présente de nombreux avantages, ne serait-ce que l'économie de moyens nécessaires au fonctionnement.
Un programme CLI sera surtout **beaucoup plus facile à écrire** dans un premier temps car nous n'aurons pas à interagir avec le serveur graphique et le gestionnaire de fenêtres et autres composants graphiques.

**Notre mission**, L'utilisateur de notre application doit maintenant pouvoir saisir la distance entre son domicile et son lieu de travail. Il doit également pouvoir choisir son mode de transport...

Puis nous essayerons de séparer les différentes parties de notre code : affichage de l'en tête, des coûts, saisie du déplacement, etc. Tout ceci se fait en créant des **fonctions**.
